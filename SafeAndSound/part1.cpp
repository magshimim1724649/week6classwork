#include "part1.h"
#include <iostream>

char *string_copy(char *dest, unsigned int destsize, const char *src)
{
    char *ret = dest;
    while (destsize-- && (*dest++ = *src++))
        ;
    *(dest-1) = 0;
    return ret;
}

void part1()
{
    char password[] = "secret";
    char dest[12];
    char src[] = "hello world!";

    string_copy(dest, 12, src);

    std::cout << src << std::endl;
    std::cout << dest << std::endl;
}
