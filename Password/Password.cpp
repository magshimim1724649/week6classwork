#include <cstring>
#include <iostream>

struct Password
{
    char value[16];
    bool incorrect;
    Password() : value(""), incorrect(true)
    {
    }
};

int main()
{
    std::cout << "Enter your password to continue:" << std::endl;
    Password pwd;
    std::cin >> pwd.value;

    // password equal to ********
    if (!strcmp(pwd.value, "********"))
        pwd.incorrect = false; // it's incorrect

    if (pwd.incorrect == false)
        std::cout << "Congratulations\n";

    return 0;
}
