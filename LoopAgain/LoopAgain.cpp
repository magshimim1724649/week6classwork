#include <iostream>

int main()
{
    int size = 0;


    std::cout << "what is the size of the series? ";
    std::cin >> size;

    int t1 = 0;
    int t2 = 1;
    while (size >= 0)
    {
        std::cout << t1 << ", ";

        int nextTerm = t1 + t2;
        t1 = t2;
        t2 = nextTerm;

        size--;
    }

    return 0;
}
