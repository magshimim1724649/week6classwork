#ifndef __TRIANGLE_H__
#define __TRIANGLE_H__

#include "shape.h"

class Triangle : public Shape
{
  private:
    float _base;
    float _height;

  public:
    Triangle(float base, float height);
    ~Triangle() override = default;
    float get_area(bool has_depth) const;
    float get_area() const override;
};

#endif // __TRIANGLE_H__
